> Nama : Muhammad Naufal Faishal            
Kelas / Absen : 3SI3 / 27

## Laporan Praktikum 14

1. Add Mahasiswa
        >![Add Mahasiswa](/images/addMahasiswa.png "Add Mahasiswa")
        
1. View Mahasiswa
        >![View Mahasiswa](/images/viewMahasiswas.png "View Mahasiswa")
        
1. Update Mahasiswa
        >![Update Mahasiswa](/images/updateMahasiswa.png "Update Mahasiswa")
        
1. Delete Mahasiswa
        >![Delete Mahasiswa](/images/deleteMahasiswa.png "Delete Mahasiswa")
        